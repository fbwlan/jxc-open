/*
 * @Descripttion:
 * @version:
 * @Author: cxguo
 * @Date: 2019-12-13 17:04:37
 * @LastEditors: cxguo
 * @LastEditTime: 2020-11-12 16:26:53
 */
import Layout from '@/layout'

export default {
  path: '/stock',
  component: Layout,
  redirect: 'noRedirect',
  meta: { title: '库存', icon: 'store' },
  children: [
    {
      path: 'purchase_list',
      name: 'PurchaseList',
      component: () => import('@/views/bill/purchase/purchase/list.vue'),
      meta: { title: '进货单', pid: 'stock1', add: true, perm: 'purchase:list' }
    },
    {
      path: 'purchase_add',
      name: 'PurchaseAdd',
      hidden: true,
      component: () => import('@/views/bill/purchase/purchase/detail.vue'),
      meta: { title: '新增进货单', pid: 'stock1', perm: 'purchase:add' }
    },
    {
      path: 'purchase_copyadd',
      name: 'PurchaseCopyAdd',
      hidden: true,
      props: (route) => ({ operation: 'copyadd', billId: route.query.billId }),
      component: () => import('@/views/bill/purchase/purchase/detail.vue'),
      meta: { title: '复制新增进货单', pid: 'stock1', perm: 'purchase:add' }
    },
    {
      path: 'purchase_detail',
      name: 'PurchaseDetail',
      hidden: true,
      props: (route) => ({ operation: 'view', billId: route.query.billId }),
      component: () => import('@/views/bill/purchase/purchase/detail-view.vue'),
      meta: { title: '进货单详情', pid: 'stock1', perm: 'purchase:add' }
    },
    {
      path: 'purchasereturn_list',
      name: 'PurchasereturnList',
      component: () => import('@/views/bill/purchase/return-purchase/list.vue'),
      meta: { title: '进货退货', pid: 'stock1', add: true, perm: 'purchase_return:list' }
    },
    {
      path: 'purchasereturn_detail',
      name: 'PurchasereturnDetail',
      hidden: true,
      props: (route) => ({ operation: 'view', billId: route.query.billId }),
      component: () => import('@/views/bill/purchase/return-purchase/detail-view.vue'),
      meta: { title: '进货退货单详情', pid: 'stock1', perm: 'purchase_return:add' }
    },
    {
      path: 'purchasereturn_add',
      name: 'PurchasereturnAdd',
      hidden: true,
      props: (route) => ({ operation: 'add' }),
      component: () => import('@/views/bill/purchase/return-purchase/detail-default.vue'),
      meta: { title: '新增进货退货单', pid: 'stock1', perm: 'purchase_return:add' }
    },
    {
      path: 'purchasereturn_copyadd',
      name: 'PurchasereturnCopyAdd',
      hidden: true,
      props: (route) => ({ operation: 'copyadd', billId: route.query.billId }),
      component: () => import('@/views/bill/purchase/return-purchase/detail.vue'),
      meta: { title: '复制新增进货退货单', pid: 'stock1', perm: 'purchase_return:add' }
    },
    {
      path: 'purchasereturnadd_hasrelation',
      name: 'PurchasereturnAddHasRelation',
      hidden: true,
      props: (route) => ({ billId: route.query.billId }),
      component: () => import('@/views/bill/purchase/return-purchase/detail-trans.vue'),
      meta: { title: '新增进货退货(关联)', pid: 'stock1', perm: 'purchase_return:add' }
    },
    {
      path: 'purchasereturnadd_norelation',
      name: 'PurchasereturnAddNoRelation',
      hidden: true,
      props: (route) => ({ operation: 'add' }),
      component: () => import('@/views/bill/purchase/return-purchase/detail.vue'),
      meta: { title: '新增进货退货(不关联)', pid: 'stock1', perm: 'purchase_return:add' }
    },
    // 盘点
    {
      path: 'checkstock_list',
      name: 'CheckstockList',
      component: () => import('@/views/bill/checkstock/list.vue'),
      meta: { title: '盘点', pid: 'stock2', add: true, perm: 'inventory:list' }
    },
    {
      path: 'checkstock_detail',
      name: 'CheckstockDetail',
      hidden: true,
      props: (route) => ({ billId: route.query.billId }),
      component: () => import('@/views/bill/checkstock/detail-view.vue'),
      meta: { title: '盘点单详情', pid: 'stock2', perm: 'inventory:add' }
    },
    {
      path: 'checkstock_add',
      name: 'CheckstockAdd',
      hidden: true,
      component: () => import('@/views/bill/checkstock/detail.vue'),
      meta: { title: '新增盘点单', pid: 'stock2', perm: 'inventory:add' }
    },
    {
      path: 'checkstock_copyadd',
      name: 'CheckstockCopyAdd',
      hidden: true,
      props: (route) => ({ operation: 'copyadd', billId: route.query.billId }),
      component: () => import('@/views/bill/checkstock/detail.vue'),
      meta: { title: '复制新增盘点单', pid: 'stock2', perm: 'inventory:add' }
    },
    {
      path: 'stockin_list',
      name: 'StockinList',
      props: (route) => ({ stockStatus: route.query.stockStatus }),
      component: () => import('@/views/bill/stock/list-in'),
      meta: { title: '入库', pid: 'stock2', perm: 'stock_in:list' }
    },
    {
      path: 'stockwaitein_detail',
      name: 'StockWaiteinDetail',
      hidden: true,
      props: (route) => ({ billId: route.query.billId }),
      component: () => import('@/views/bill/stock/list-in/waite-detail-view.vue'),
      meta: { title: '待入库单详情', pid: 'stock2', perm: 'stock_in:add' }
    },
    {
      path: 'stockin_add',
      hidden: true,
      name: 'StockinAdd',
      props: (route) => ({ direction: '1', billId: route.query.billId, billCat: route.query.billCat }),
      component: () => import('@/views/bill/stock/list-in/detail-trans.vue'),
      meta: { title: '新增入库单', pid: 'stock2', perm: 'stock_in:add' }
    },
    {
      path: 'stockin_hasdetailview',
      hidden: true,
      name: 'StockInHasDetailView',
      props: (route) => ({ direction: '1', billId: route.query.billId, billCat: route.query.billCat }),
      component: () => import('@/views/bill/stock/list-in/has-detail-view.vue'),
      meta: { title: '入库单详情', pid: 'stock2', perm: 'stock_in:add' }
    },
    {
      path: 'stockout_list',
      name: 'StockoutList',
      props: (route) => ({ stockStatus: route.query.stockStatus }),
      component: () => import('@/views/bill/stock/list-out'),
      meta: { title: '出库', pid: 'stock2', perm: 'stock_out:list' }
    },
    {
      path: 'stockout_add',
      name: 'StockoutAdd',
      hidden: true,
      props: (route) => ({ direction: '0', billId: route.query.billId, billCat: route.query.billCat }),
      component: () => import('@/views/bill/stock/list-out/detail-trans.vue'),
      meta: { title: '新增出库单', pid: 'stock2', perm: 'stock_out:add' }
    },
    {
      path: 'stockout_hasdetailview',
      hidden: true,
      name: 'StockOutHasDetailView',
      props: (route) => ({ direction: '1', billId: route.query.billId, billCat: route.query.billCat }),
      component: () => import('@/views/bill/stock/list-out/has-detail-view.vue'),
      meta: { title: '出库单详情', pid: 'stock2', perm: 'stock_out:add' }
    },
    {
      path: 'stock_list',
      name: 'StockList',
      component: () => import('@/views/stock'),
      meta: { title: '库存查询', pid: 'stock3', perm: 'stock_query:list' }
    },
    {
      path: 'stock_log_list',
      name: 'StockLogList',
      hidden: true,
      component: () => import('@/views/stock/good-stock-log.vue'),
      props: (route) => ({ skuId: route.query.id, skuName: route.query.name }),
      meta: { title: '库存流水明细', pid: 'stock3', perm: 'stock_query:list' }
    },
    {
      path: 'costprice_log_list',
      name: 'CostPriceLogList',
      hidden: true,
      component: () => import('@/views/stock/list-costprice-bygood.vue'),
      props: (route) => ({ skuId: route.query.id, skuCode: route.query.code, skuName: route.query.name }),
      meta: { title: '成本价明细', pid: 'stock3', perm: 'stock_query:list' }
    },
    {
      path: 'stock_warn',
      name: 'StockWarn',
      hidden: true,
      component: () => import('@/views/stock-warn'),
      meta: { title: '库存预警', pid: 'stock3', perm: 'stock_query:list' }
    }
  ]
}
