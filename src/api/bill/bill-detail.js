/*
 * @Descripttion: 票据接口
 * @version:
 * @Author: cxguo
 * @Date: 2019-08-02 15:03:05
 * @LastEditors  : cxguo
 * @LastEditTime : 2020-01-18 00:19:58
 */
import axios from '@/utils/request'
const baseUrl = '/bill_detail'

/**
 * 查询已转明细
 * @param {*} params
 */
export function getPurchaseOrderHasTransDetail(params) {
  return axios.request({
    url: `${baseUrl}/get_phuraseorder_hastrans`,
    method: 'post',
    data: params
  })
}

/**
 * 查询未转明细
 * @param {*} params
 */
export function getPurchaseOrderNotTransDetail(params) {
  return axios.request({
    url: `${baseUrl}/get_phuraseorder_nottrans`,
    method: 'post',
    data: params
  })
}
