/*
 * @Descripttion: 进出库票据接口
 * @version:
 * @Author: cxguo
 * @Date: 2019-08-02 15:03:05
 * @LastEditors: cxguo
 * @LastEditTime: 2020-11-05 14:18:35
 */
import axios from '@/utils/request'
const baseUrl = '/bill/stockIn'

/**
 * 新增入库单
 * @param {*} params
 */
export function addStockIn(params) {
  return axios.request({
    url: `${baseUrl}/addData`,
    method: 'post',
    data: params
  })
}

export function cancleBill(params) {
  return axios.request({
    url: `${baseUrl}/cancle`,
    method: 'post',
    data: params
  })
}

/**
 * 查询待入库单子
 * @param {*} params
 */
export function getWaiteStockInBillList(params) {
  return axios.request({
    url: `${baseUrl}/listWaite2InBillsPage`,
    method: 'post',
    data: params
  })
}

export function getWaiteBillDetail(billId) {
  return axios.request({
    url: `${baseUrl}/listWaiteInGoods/${billId}`,
    method: 'post',
    data: billId
  })
}

/**
 * 查询已入库单子
 * @param {*} params
 */
export function getHasStockInBillList(params) {
  return axios.request({
    url: `${baseUrl}/listHasInBillsPage`,
    method: 'post',
    data: params
  })
}

export function getWaiteGoodsByBillId(billId) {
  return axios.request({
    url: `${baseUrl}/listWaiteInGoods/${billId}`,
    method: 'post',
    data: billId
  })
}

export function getHasInGoodsByBillId(billId) {
  return axios.request({
    url: `${baseUrl}/listHasInGoods/${billId}`,
    method: 'post',
    data: billId
  })
}

export function getBillcode(params) {
  return axios.request({
    url: `${baseUrl}/getBillNo`,
    method: 'post',
    data: params
  })
}

export function getBillDetail(billId) {
  return axios.request({
    url: `${baseUrl}/getBillAndDetails/${billId}`,
    method: 'post',
    data: billId
  })
}

export function print(params) {
  return axios.request({
    url: `${baseUrl}/print`,
    method: 'post',
    data: params
  })
}

