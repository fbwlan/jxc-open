/*
 * @Descripttion: 账户
 * @version:
 * @Author: cxguo
 * @Date: 2019-08-02 15:03:05
 * @LastEditors: cxguo
 * @LastEditTime: 2020-11-07 09:32:19
 */
import axios from '@/utils/request'
const baseUrl = '/guide'

export function saveData(params) {
  return axios.request({
    url: `${baseUrl}/saveData`,
    method: 'post',
    data: params
  })
}

export function delData(id) {
  return axios.request({
    url: `${baseUrl}/delete/${id}`,
    method: 'post',
    data: id
  })
}

export function listData(params) {
  return axios.request({
    url: `${baseUrl}/listPage`,
    method: 'post',
    data: params
  })
}

