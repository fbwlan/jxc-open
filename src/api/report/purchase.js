/*
 * @Descripttion: 客户接口
 * @version:
 * @Author: cxguo
 * @Date: 2019-08-02 15:03:05
 * @LastEditors: cxguo
 * @LastEditTime: 2020-11-06 08:33:02
 */
import axios from '@/utils/request'
const baseUrl = '/report/purchase'

export function getPurchaseInfo(params) {
  return axios.request({
    url: `${baseUrl}/getInfo`,
    method: 'post',
    data: params
  })
}

export function listPurchaseGroupByBill(params) {
  return axios.request({
    url: `${baseUrl}/listDataGroupByBillPage`,
    method: 'post',
    data: params
  })
}

export function listPurchaseGroupByComego(params) {
  return axios.request({
    url: `${baseUrl}/listDataGroupByComegoPage`,
    method: 'post',
    data: params
  })
}

export function listDataGroupByGoods(params) {
  return axios.request({
    url: `${baseUrl}/listDataGroupByGoodPage`,
    method: 'post',
    data: params
  })
}

export function detailGoodsGroupByBill(params) {
  const skuId = params.data.skuId
  return axios.request({
    url: `${baseUrl}/listGoodDetailGroupByBill/${skuId}`,
    method: 'post',
    data: params
  })
}

export function detailGoodsGroupByComego(params) {
  const skuId = params.data.skuId
  return axios.request({
    url: `${baseUrl}/listGoodDetailGroupByComego/${skuId}`,
    method: 'post',
    data: params
  })
}

export function detailComegoGroupByBill(params) {
  const comegoId = params.data.comegoId
  return axios.request({
    url: `${baseUrl}/listComegoDetailGroupByBill/${comegoId}`,
    method: 'post',
    data: params
  })
}

export function detailComegoGroupByGoods(params) {
  const comegoId = params.data.comegoId
  return axios.request({
    url: `${baseUrl}/listComegoDetailGroupByGood/${comegoId}`,
    method: 'post',
    data: params
  })
}

export function listBillDetailByDate(params) {
  const date = params.data.date
  return axios.request({
    url: `${baseUrl}/listBillDetail/${date}`,
    method: 'post',
    data: params
  })
}
