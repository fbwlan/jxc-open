/*
 * @Descripttion: 客户接口
 * @version:
 * @Author: cxguo
 * @Date: 2019-08-02 15:03:05
 * @LastEditors: cxguo
 * @LastEditTime: 2020-11-06 09:56:24
 */
import axios from '@/utils/request'
const baseUrl = '/report/fin'

export function getRece(params) {
  return axios.request({
    url: `${baseUrl}/getReceAmount`,
    method: 'post',
    data: params
  })
}

export function getPay(params) {
  return axios.request({
    url: `${baseUrl}/getPayAmount`,
    method: 'post',
    data: params
  })
}

export function listPerformance(params) {
  return axios.request({
    url: `${baseUrl}/listPerformanceGroupByUser`,
    method: 'post',
    data: params
  })
}

export function getStatisticsInfo(params) {
  return axios.request({
    url: `${baseUrl}/getPerformanceInfo`,
    method: 'post',
    data: params
  })
}

