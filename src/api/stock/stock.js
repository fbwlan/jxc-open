/*
 * @Descripttion: 库存接口
 * @version:
 * @Author: cxguo
 * @Date: 2019-08-02 15:03:05
 * @LastEditors: cxguo
 * @LastEditTime: 2020-11-12 14:43:00
 */
import axios from '@/utils/request'
const baseUrl = '/stockQuery'

/**
 * 查询库存列表
 * @param {*} params
 */
export function getStockList(params) {
  return axios.request({
    url: `${baseUrl}/listPage`,
    method: 'post',
    data: params
  })
}

/**
 * 查询某商品的库存状态
 * @param {*} skuId
 */
export function getStockBySkuId(skuId) {
  return axios.request({
    url: `${baseUrl}/getStock/${skuId}`,
    method: 'post',
    data: skuId
  })
}

