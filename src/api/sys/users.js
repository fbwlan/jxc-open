/*
 * @Descripttion: 角色接口
 * @version:
 * @Author: cxguo
 * @Date: 2019-08-02 15:03:05
 * @LastEditors: cxguo
 * @LastEditTime: 2020-11-06 21:30:40
 */
import axios from '@/utils/request'
const baseUrl = '/user'

export function register(params) {
  return axios.request({
    url: `${baseUrl}/register`,
    method: 'post',
    data: params
  })
}

export function login(params) {
  return axios.request({
    url: `${baseUrl}/login`,
    method: 'post',
    data: params
  })
}

export function delData(params) {
  return axios.request({
    url: `${baseUrl}/delListData`,
    method: 'post',
    data: params
  })
}

export function updateRoles(params) {
  return axios.request({
    url: `${baseUrl}/updateRoles`,
    method: 'post',
    data: params
  })
}

/**
 * 查询子账号
 * @param {*} params
 */
export function listSonData(params) {
  return axios.request({
    url: `${baseUrl}/listSonData`,
    method: 'post',
    data: params
  })
}

/**
 * 查询用户的角色Id
 * @param {*} params
 */
export function getUserRoleIds(params) {
  return axios.request({
    url: `${baseUrl}/listUserRoles`,
    method: 'post',
    data: params
  })
}

export function getUserInfo() {
  return axios.request({
    url: `${baseUrl}/getUserInfo`,
    method: 'post'
  })
}

export function initApp() {
  return axios.request({
    url: `${baseUrl}/initApp`,
    method: 'post'
  })
}

export function isExistUsername(params) {
  return axios.request({
    url: `${baseUrl}/isExistUsername`,
    method: 'post',
    data: params
  })
}

export function isExistPhoneNo(params) {
  return axios.request({
    url: `${baseUrl}/isExistPhone`,
    method: 'post',
    data: params
  })
}

