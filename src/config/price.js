/*
 * @Descripttion: 价格相关的数据和方法
 * @version:
 * @Author: cxguo
 * @Date: 2020-04-05 10:16:40
 * @LastEditors: cxguo
 * @LastEditTime: 2020-08-15 15:18:16
 */
import store from '@/store'

export const getLabelByVal = function(val) {
  const priceType = store.getters.appData.configs.priceTypes
  const obj = priceType.find(item => { return item.value === val })
  return obj.label
}

export const getPriceOptions = function(priceStr, priceCat) {
  const priceObj = JSON.parse(priceStr)
  const result = []
  Object.keys(priceObj).forEach(key => {
    if (priceCat.indexOf(key) >= 0) {
      const obj = {
        value: key,
        label: `${getLabelByVal(key)}:${priceObj[key]}`,
        price: `${priceObj[key]}`
      }
      result.push(obj)
    }
  })
  return result
}
