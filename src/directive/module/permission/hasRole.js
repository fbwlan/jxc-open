/*
 * @Descripttion:
 * @version:
 * @Author: cxguo
 * @Date: 2019-08-19 09:17:15
 * @LastEditors: cxguo
 * @LastEditTime: 2019-08-19 09:20:10
 */
import { getCookie } from '@/libs/util.js'

/**
 * 判断是否有角色
 * @param {角色} role
 */
export default (role) => {
  const data = getCookie()
  const roleList = data.ROLELIST
  if (!roleList) return false
  return roleList.indexOf(role) !== -1
}
