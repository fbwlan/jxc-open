/*
 * @Descripttion:
 * @version:
 * @Author: cxguo
 * @Date: 2019-07-04 01:36:52
 * @LastEditors: cxguo
 * @LastEditTime: 2020-12-24 21:25:46
 */
import Vue from 'vue'

import 'normalize.css/normalize.css' // A modern alternative to CSS resets
// import 'http://at.alicdn.com/t/font_1404534_oid6onxs8pr.css'
import ElementUI from 'element-ui'
import { Loading } from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import locale from 'element-ui/lib/locale/lang/en' // lang i18n
import '@/styles/index.scss' // global css
import '@/styles/components.scss' // global css
import App from './App'
import store from './store'
import router from './router'
import importDirective from '@/directive'
import i18n from './lang' // internationalization
import './icons' // icon
import '@/permission' // permission control
import { amount } from '@/libs/tools.js'
import 'xe-utils'
import VXETable from 'vxe-table'
import VXETablePluginElement from 'vxe-table-plugin-element'
import 'vxe-table-plugin-element/dist/style.css'
import 'vxe-table/lib/style.css'
import { installVxeRenders } from '@/utils/vxe-table'
import { installCardDragger } from '@/components/DragComponent'
import layer from '@/components/packages'

Vue.use(installCardDragger)
/**
 * If you don't want to use mock-server
 * you want to use MockJs for mock api
 * you can execute: mockXHR()
 *
 * Currently MockJs will be used in the production environment,
 * please remove it before going online! ! !
 */

importDirective(Vue)
Vue.use(layer)
// set ElementUI lang to EN
Vue.use(ElementUI, {
  locale, size: 'mini', zIndex: 3000,
  i18n: (key, value) => i18n.t(key, value)
})

Vue.use(VXETable)
VXETable.use(VXETablePluginElement)
VXETable.setup({
  size: 'mini',
  table: {
    keepSource: false
  }
})
installVxeRenders(VXETable)

/**
 * 配置layer参数
 */
// Vue.use(layer)

Vue.prototype.$store = store
Vue.prototype.$amount = amount
Vue.prototype.$loading = Loading.service
Vue.prototype.$bus = new Vue()

Vue.config.productionTip = false
Vue.config.devtools = true

new Vue({
  el: '#app',
  store,
  router,
  i18n,
  render: h => h(App)
})
