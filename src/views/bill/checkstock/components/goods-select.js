/*
 * @Descripttion:
 * @version:
 * @Author: cxguo
 * @Date: 2020-04-16 22:20:19
 * @LastEditors: cxguo
 * @LastEditTime: 2020-07-24 13:08:02
 */
import GoodsSelect from '@/views/goods-select'
import { sunTableAmount } from '@/libs/util.js'

export default {
  name: 'CheckStockSelectGoods',
  extends: GoodsSelect,
  mixins: [GoodsSelect],
  props: {},
  data() {
    return {
      columnsQty: [
        { field: 'bookQty', title: '账面数量', width: 100 },
        { field: 'realQty', title: '实际数量', width: 100,
          editRender: { name: 'CxInputNumber', props: { size: 'mini', min: 0 }}
        }
      ]
    }
  },
  created() {
    const originColumnsData = this.originColumnsData
    const excludeCols = ['quantity', 'price', 'total']
    const filterCols = originColumnsData.filter(item => { return excludeCols.indexOf(item.field) === -1 })
    // 新增
    const columnsQty = this.columnsQty
    const remarkIndex = filterCols.findIndex(item => { return item.field === 'remark' })
    filterCols.splice(remarkIndex, 0, ...columnsQty)
    // 修改商品下拉框cols
    const goodCol = filterCols.find(item => { return item.field === 'name' })
    const formatColumnsData = function(cols) {
      const col = cols.find(item => { return item.field === 'canUseQty' })
      col.field = 'nowQty'
      col.title = '当前存货'
      return cols
    }
    goodCol.editRender.methods = { formatColumnsData }
    this.originColumnsData = filterCols
  },
  methods: {
    handleTableActiveCell(row) {
      this.setTableActiveCell(row, 'realQty')
    },
    formatGoodListData(data) {
      data.forEach(item => {
        item.bookQty = item.nowQty
        item.realQty = Number(item.nowQty)
      })
      return data
    },
    footerMethod({ columns, data }) {
      return [
        columns.map((column, columnIndex) => {
          if (columnIndex === 0) {
            return '合计'
          }
          if (['bookQty', 'realQty'].includes(column.property)) {
            return sunTableAmount(data, column.property)
          }
          return null
        })
      ]
    },
    isSkuExsite(skuId) {
      const tableData = this.getTableData().tableData || []
      for (let i = 0; i < tableData.length; i++) {
        const item = tableData[i]
        if (item.skuId === skuId) return true
      }
      return false
    }
  }
}
