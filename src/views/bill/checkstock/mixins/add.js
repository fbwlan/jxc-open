
/*
 * @Descripttion: 客户的跳转
 * @version:
 * @Author: cxguo
 * @Date: 2019-08-16 09:17:57
 * @LastEditors: cxguo
 * @LastEditTime: 2020-08-17 15:55:21
 */
import moment from 'moment'
import { getBillNo } from '@/api/bill/bill-inventory.js'
export default {
  data() {
    return {}
  },
  methods: {
    initSaveData() {
      const date = moment().format('YYYY-MM-DD HH:mm:ss')
      this.dataObj = {
        businessTime: date,
        handUserId: this.currentUserId,
        amountOther: '0.00'
      }
      this.getBillCode()
    },
    getBillCode() {
      getBillNo().then(res => {
        if (!res.data.flag) this.$message.error('获取票据编号失败！')
        const code = res.data.data
        this.$set(this.dataObj, 'billNo', code)
      })
    }
  }
}

