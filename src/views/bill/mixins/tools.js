/*
 * @Descripttion: 客户的跳转
 * @version:
 * @Author: cxguo
 * @Date: 2019-08-16 09:17:57
 * @LastEditors: cxguo
 * @LastEditTime: 2020-04-12 17:34:49
 */
export default {
  data() {
    return {}
  },
  methods: {
    onComegoChange(data) {
      const { id, name } = data
      this.$set(this.dataObj, 'comegoId', id)
      this.$set(this.dataObj, 'comegoName', name)
      this.isShowComegoList = false
    }
  }
}

