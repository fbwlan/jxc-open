
/*
 * @Descripttion: 客户的跳转
 * @version:
 * @Author: cxguo
 * @Date: 2019-08-16 09:17:57
 * @LastEditors: cxguo
 * @LastEditTime: 2020-08-20 08:54:31
 */
import { getBillAndDetail } from '@/api/bill/bill-sale.js'

export default {
  data() {
    return {}
  },
  methods: {
    initUpdateData() {
      const billId = this.billId
      this.initOptions()
      return this.getBillContainGoodsData(billId).then(data => {
        const { bill, billExtSale, billDetailList } = data
        this.dataObj = bill
        this.billExtSale = billExtSale
        this.$refs.GoodsSelect.setTableData(billDetailList)
      })
    },
    initViewData() {
      const billId = this.billId
      return this.getBillContainGoodsData(billId).then(data => {
        const { bill, billExtSale, billDetailList } = data
        this.dataObj = bill
        this.billExtSale = billExtSale
        this.$refs.GoodsSelect.setTableData(billDetailList)
      })
    },
    initTransData() {
      this.getBillCode()
      const billId = this.billId
      return this.getBillContainGoodsData(billId).then(data => {
        const { bill, billExtSale, billDetailList } = data
        bill.saleBillNo = bill.billNo
        bill.billRelationId = bill.id
        bill.billRelationNo = bill.billNo
        delete bill.billNo
        delete bill.id
        this.billExtSale = billExtSale
        this.setDataObj(bill)
        billDetailList.forEach(item => {
          const { quantity, changeQuantity } = item
          const canTransQty = this.$amount(quantity).subtract(changeQuantity).format()
          this.$set(item, 'quantity', canTransQty)
        })
        this.$refs.GoodsSelect.setTableData(billDetailList)
      })
    },
    setDataObj(dataObj) {
      Object.keys(dataObj).forEach(key => {
        dataObj[key] && this.$set(this.dataObj, key, dataObj[key])
      })
    },
    getBillContainGoodsData(billId) {
      return new Promise((resolve, reject) => {
        getBillAndDetail(billId).then(res => {
          if (!res.data.flag) {
            return reject(new Error('获取采购单商品失败！'))
          }
          const data = res.data.data
          data._amountDiscount = data.amountDiscount
          resolve(data)
        }).catch(err => {
          reject(err)
        })
      })
    }
  }
}

