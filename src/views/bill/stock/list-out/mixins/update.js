
/*
 * @Descripttion: 客户的跳转
 * @version:
 * @Author: cxguo
 * @Date: 2019-08-16 09:17:57
 * @LastEditors: cxguo
 * @LastEditTime: 2020-09-10 15:30:18
 */
import { getWaiteDetail as getBillAndDetail } from '@/api/bill/bill-stock-out.js'

export default {
  data() {
    return {}
  },
  methods: {
    initUpdateData() {
      const billId = this.billId
      return this.getBillContainGoodsData(billId).then(data => {
        const { bill, billDetailList } = data
        this.dataObj = bill
        this.$refs.GoodsSelect.setTableData(billDetailList)
      })
    },
    initViewData() {
      const billId = this.billId
      return this.getBillContainGoodsData(billId).then(data => {
        const { bill, billDetailList } = data
        Object.keys(bill).forEach(key => {
          this.$set(this.dataObj, key, bill[key])
        })
        this.$refs.GoodsSelect.setTableData(billDetailList)
      })
    },
    initTransData() {
      const billId = this.billId
      return this.getBillContainGoodsData(billId).then(data => {
        const { bill, billDetailList } = data
        bill.billRelationId = bill.id
        bill.billRelationNo = bill.billNo
        billDetailList.forEach(item => {
          const { quantity, changeQuantity } = item
          const targetQ = Number(this.$amount(quantity).subtract(changeQuantity).format())
          item.quantity = targetQ
          item.maxq = targetQ
        })
        this.setDataObj(bill)
        this.$refs.GoodsSelect.setTableData(billDetailList)
      })
    },
    setDataObj(dataObj) {
      Object.keys(dataObj).forEach(key => {
        this.$set(this.dataObj, key, dataObj[key])
      })
    },
    getBillContainGoodsData(billId) {
      const params = {
        billId,
        billCat: this.billCat
      }
      return new Promise((resolve, reject) => {
        getBillAndDetail(params).then(res => {
          if (!res.data.flag) {
            return reject(new Error('获取采购单商品失败！'))
          }
          const data = res.data.data
          resolve(data)
        }).catch(err => {
          reject(err)
        })
      })
    }
  }
}

