/*
 * @Descripttion:
 * @version:
 * @Author: cxguo
 * @Date: 2020-06-10 13:45:41
 * @LastEditors: cxguo
 * @LastEditTime: 2020-06-12 11:43:06
 */

/**
  * 根据后台返回的价格数据，转化成前端需要的数据
  * @param {价格数据} data
  */
export const getPriceList = function(skuData) {
  const data = skuData
  const { unitId, unitName, price } = data
  if (!unitId) return []
  const unitIdArr = unitId.split(',')
  const unitNameArr = unitName.split(',')
  const priceArr = getPriceArr(price)
  const result = []
  unitIdArr.forEach((val, i) => {
    const obj = { unitId: val, unitName: unitNameArr[i], price: priceArr[i] }
    result.push(obj)
  })
  return result
}

export const getBarcodeList = function(skuData) {
  const data = skuData
  const { unitId, unitName, barcode } = data
  if (!unitId) return []
  const unitIdArr = unitId.split(',')
  const unitNameArr = unitName.split(',')
  const barcodeArr = barcode ? barcode.split(',') : []
  const result = []
  unitIdArr.forEach((val, i) => {
    const obj = { unitId: val, unitName: unitNameArr[i], barcode: barcodeArr[i] }
    result.push(obj)
  })
  return result
}

const getPriceArr = function(priceJson) {
  const priceJsonArr = `[${priceJson}]`
  const priceArr = JSON.parse(priceJsonArr)
  const arr = priceArr.map(item => {
    return JSON.stringify(item)
  })
  return arr
}
