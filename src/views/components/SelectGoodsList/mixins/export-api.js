
/*
 * @Descripttion: 客户的跳转
 * @version:
 * @Author: cxguo
 * @Date: 2019-08-16 09:17:57
 * @LastEditors: cxguo
 * @LastEditTime: 2020-07-07 08:25:33
 */
export default {
  data() {
    return {}
  },
  methods: {
    resetData() {
      this.$refs.goodsList.resetData()
    },
    setAllCheckboxRow(b) {
      this.$refs.goodsList.setAllCheckboxRow(b)
    },
    getSelectData() {
      const vm = this.$refs.goodsList
      if (!vm) return []
      return vm.getCheckboxRecords()
    }
  }
}

