
/*
 * @Descripttion: 客户的跳转
 * @version:
 * @Author: cxguo
 * @Date: 2019-08-16 09:17:57
 * @LastEditors: cxguo
 * @LastEditTime: 2020-06-21 17:56:59
 */
import { getBillSaleContainGoodsData } from '@/api/bill/bill.js'

export default {
  data() {
    return {}
  },
  methods: {
    initUpdateData() {
      const billId = this.billId
      return this.getBillContainGoodsData(billId).then(data => {
        const { bill, billExtSale, billdetailList } = data
        Object.keys(bill).forEach(key => {
          this.$set(this.dataObj, key, bill[key])
        })
        this.billExtSale = billExtSale || {}
        billdetailList.forEach(item => {
          item._ok = true
        })
        this.$refs.GoodsSelect.setTableData(billdetailList)
      })
    },
    initTransData() {
      this.getAccount()
      const billId = this.billId
      return this.getBillContainGoodsData(billId).then(data => {
        const { bill, billExtSale, billdetailList } = data
        bill.borrowbillNo = bill.billNo
        this.dataObj = bill
        this.billExtSale = billExtSale || { type: '0' }
        billdetailList.forEach(item => {
          item.originQuantity = item.quantity
          const { changeQuantity, returnQuantity, changeQuantity2 } = item
          const canUseQ = changeQuantity - (returnQuantity || 0 + changeQuantity2 || 0)
          item.quantity = canUseQ
          item.maxq = canUseQ
          item._ok = true
        })
        this.$refs.GoodsSelect.setTableData(billdetailList)
      })
    },
    initViewData() {
      const billId = this.billId
      return this.getBillContainGoodsData(billId).then(data => {
        const { bill, billExtSale, billdetailList } = data
        Object.keys(bill).forEach(key => {
          this.$set(this.dataObj, key, bill[key])
        })
        this.billExtSale = billExtSale
        billdetailList.forEach(item => {
          item._ok = true
        })
        this.$refs.GoodsSelect.setTableData(billdetailList)
      })
    },
    getBillContainGoodsData(billId) {
      return new Promise((resolve, reject) => {
        getBillSaleContainGoodsData(billId).then(res => {
          if (!res.data.flag) {
            return reject(new Error('获取采购单商品失败！'))
          }
          const data = res.data.data
          resolve(data)
        }).catch(err => {
          reject(err)
        })
      })
    }
  }
}

