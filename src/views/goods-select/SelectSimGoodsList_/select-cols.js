/*
 * @Descripttion: 下拉框的数据
 * @version:
 * @Author: cxguo
 * @Date: 2019-08-16 09:17:57
 * @LastEditors: cxguo
 * @LastEditTime: 2020-07-23 21:39:15
 */
export default {
  data() {
    return {
      columnsData: [
        { field: 'code', title: '商品编号', width: 150 },
        { field: 'name', title: '商品名称/规格', width: 200 },
        { field: 'unitName', title: '单位', width: 140 }
      ],
      columnsStock: [
        { field: 'canUseQty', title: '可用库存', width: 100 }
      ]
    }
  },
  methods: {
  }
}

