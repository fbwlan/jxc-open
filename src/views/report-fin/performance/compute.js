/*
 * @Descripttion: 金额数量等的计算方法
 * @version:
 * @Author: cxguo
 * @Date: 2020-05-22 13:28:56
 * @LastEditors: cxguo
 * @LastEditTime: 2020-09-01 13:55:56
 */

/**
 * 获取销售毛利率
 */
export const getGrossProfitPerMethod = function(row) {
  const { amountPayable, costPriceTotal } = row
  if (Number(amountPayable) === 0) return ''
  const grossProfit = this.$amount(amountPayable).subtract(costPriceTotal).format()
  return (Math.round(grossProfit * 10000 / amountPayable) / 100) + '%'
}

/**
 * 毛利比重
 */
export const grossProfitProportionMethod = function(row) {
  const { amountPayable, costPriceTotal } = row
  const grossProfit = this.$amount(amountPayable).subtract(costPriceTotal).format()
  const grossProfitTotal = this.statisticsInfo.grossProfit
  return (Math.round(grossProfit * 10000 / grossProfitTotal) / 100) + '%'
}

/**
 * 获取退货率
 */
export const grossReturnPerMethod = function(row) {
  console.log(qtyInBase, returnQtyInBase)
  const { qtyInBase, returnQtyInBase } = row
  if (qtyInBase === 0) return '0%'
  return (Math.round(returnQtyInBase * 10000 / qtyInBase) / 100) + '%'
}

/**
 * 获取销售比重
 * @param {*} row
 */
export const getSaleProportionMethod = function(row) {
  const { amountPayable } = row
  const amountPayableTotal = this.statisticsInfo.amountPayable
  return (Math.round(amountPayable * 10000 / amountPayableTotal) / 100) + '%'
}
